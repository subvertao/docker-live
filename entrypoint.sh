#!/bin/bash
set -xe
: "${DOMAIN?Need a domain to use in the URL}"

sed -i "s/DOMAIN_REPLACE/$DOMAIN/g" /var/www/index.html

exec "$@"
