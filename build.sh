#!/bin/bash
#
# Packages required to serve the website and run the services.
PACKAGES="nginx libnginx-mod-rtmp ffmpeg"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

apt-get clean && apt-get update

# Install required packages
install_packages ${BUILD_PACKAGES} ${PACKAGES} \
    || die "could not install packages"

# Set the localtime to Europe/Oslo
mv /etc/localtime /etc/localtime.utc
ln -s /usr/share/zoneinfo/Europe/Oslo /etc/localtime

cp -r /tmp/files/html/* /var/www/

cp /tmp/files/nginx.conf /etc/nginx/nginx.conf

# Remove packages used for installation.
apt-get autoremove -y
apt-get clean
rm -rf /tmp/files
rm -fr /var/lib/apt/lists/*

