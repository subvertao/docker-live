FROM debian:buster-slim

COPY files /tmp/files
COPY build.sh /tmp/build.sh
RUN /tmp/build.sh && rm /tmp/build.sh
COPY entrypoint.sh /tmp/entrypoint.sh
RUN chmod +x /tmp/entrypoint.sh

ENTRYPOINT ["/tmp/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
