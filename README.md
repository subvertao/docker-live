# docker-live

Build the container:

```
docker build -t live .
```

Launch the container in this fashion: 

```
docker run -e DOMAIN='localhost' -p 80:80 -p 1935:1935 -d live
```

The variable DOMAIN is the url you wish to use for your live container.
This has to be populated in your docker run in order to be able to visit the frontend.

Following the example (that is tailored for running your docker container locally) you
should be able to visit http://localhost and find live frontend running there.
You find more instructions on how to use it at the main page.

In order to stream to your live container, you can use [OBS studio](https://obsproject.com/).
In the settings section, choose service of type "custom", under "server" type rtmp://localhost/ingest/
if you are running a local container on your laptop following the example. As a "stream key", choose
a string that you will use to see the stream live: for example if you use "test" as a stream key,
in your local running docker container you will see the stream at the address http://localhost/#test

